package com.giffian.collection;

import java.util.Date;

public class Comment {

	private User user;

	private String comment;

	private Date time;

	private boolean isEdited;

	public Comment() {
		super();
	}

	public Comment(User user, String comment, Date time, boolean isEdited) {
		super();
		this.user = user;
		this.comment = comment;
		this.time = time;
		this.isEdited = isEdited;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean isEdited() {
		return isEdited;
	}

	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}

	@Override
	public String toString() {
		return "Comment [user=" + user + ", comment=" + comment + ", time="
				+ time + ", isEdited=" + isEdited + "]";
	}
}
