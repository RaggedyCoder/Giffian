package com.giffian.collection;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "feed")
public class Feed {

	private ObjectId id;
	
	private String url;

	private User user;

	private String title;

	private List<String> tags;

	private List<Comment> comments;

	private Integer upVote;

	private Integer downVote;

	private Date postTime;

	public Feed() {
		super();
	}

	public Feed(String url, User user, String title, List<String> tags,
			List<Comment> comments, Integer upVote, Integer downVote,
			Date postTime) {
		super();
		this.url = url;
		this.user = user;
		this.title = title;
		this.tags = tags;
		this.comments = comments;
		this.upVote = upVote;
		this.downVote = downVote;
		this.postTime = postTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Integer getUpVote() {
		return upVote;
	}

	public void setUpVote(Integer upVote) {
		this.upVote = upVote;
	}

	public Integer getDownVote() {
		return downVote;
	}

	public void setDownVote(Integer downVote) {
		this.downVote = downVote;
	}

	public Date getPostTime() {
		return postTime;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	@Override
	public String toString() {
		return "Feed [url=" + url + ", user=" + user + ", title=" + title
				+ ", tags=" + tags + ", comments=" + comments + ", upVote="
				+ upVote + ", downVote=" + downVote + ", postTime=" + postTime
				+ "]";
	}

}
