package com.giffian.collection;

import javax.validation.constraints.Pattern;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
public class User {

	@Id
	private ObjectId id;

	private String firstName;
	private String lastName;

	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+!=])(?=\\S+$).{8,}$")
	private String password;

	private String hashedPassword;

	private String salt;

	private String email;

	private boolean verified;

	public User() {
		super();
	}

	public User(ObjectId id, String firstName, String lastName, String password,
			String hashedPassword, String salt, String email, boolean verified) {
		super();
		this.id = id;

		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.hashedPassword = hashedPassword;
		this.salt = salt;
		this.email = email;
		this.verified = verified;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", password=" + password + ", hashedPassword="
				+ hashedPassword + ", salt=" + salt + ", email=" + email
				+ ", verified=" + verified + "]";
	}

}
