package com.giffian.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.giffian.service.DummyFeedService;

@Controller
public class GifFeedController {

	@Autowired
	private DummyFeedService dummyFeedService;

	@RequestMapping(value = { "/", "/feed" }, method = RequestMethod.GET)
	public String showFeed(Model model) {
		model.addAttribute("dummyFeedList", dummyFeedService.getFakeFeeds());
		return "timeline";
	}
}
