package com.giffian.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.giffian.service.UserService;

/**
 * Created by sajid on 1/1/2016.
 */

@Controller
public class StartupController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String startup(Model model) {
		System.out.println("[/login]");
		model.addAttribute("helloWorld", "কনগ্রেচুলেশন অন ইয়োর ব্রিলিয়ান্ট সাকসেস.");
		return "login";
	}
}
