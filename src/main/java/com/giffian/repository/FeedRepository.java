package com.giffian.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.giffian.collection.Feed;

public interface FeedRepository extends MongoRepository<Feed, ObjectId> {

}
