package com.giffian.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.giffian.collection.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

	@Query("{username : ?0}")
	public User findByUsername(String username);
}
