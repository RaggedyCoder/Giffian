package com.giffian.service;

import java.util.List;

import com.giffian.collection.Feed;

public interface DummyFeedService {

	
	public List<Feed> getFakeFeeds();
}
