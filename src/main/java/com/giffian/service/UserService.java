package com.giffian.service;

import java.util.List;

import com.giffian.collection.User;

public interface UserService {

	public User findByUsername(String username);

	public List<User> findAll();
}
