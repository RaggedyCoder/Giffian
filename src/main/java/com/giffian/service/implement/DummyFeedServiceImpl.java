package com.giffian.service.implement;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.giffian.collection.Feed;
import com.giffian.service.DummyFeedService;

@Service
public class DummyFeedServiceImpl implements DummyFeedService {

	@Override
	public List<Feed> getFakeFeeds() {
		List<Feed> feedList = new ArrayList<Feed>();
		feedList.add(new Feed(
				"https://media.giphy.com/media/l2JJHdKaR3agT2MVi/giphy.gif",
				null, "Awsome Gif", null, null, new Random().nextInt(420),
				new Random().nextInt(420), new GregorianCalendar().getTime()));

		feedList.add(new Feed(
				"https://media.giphy.com/media/9kGhitJvG6huE/giphy.gif", null,
				"Happy Easter", null, null, new Random().nextInt(420),
				new Random().nextInt(420), new GregorianCalendar().getTime()));

		feedList.add(new Feed(
				"https://media.giphy.com/media/l2QZWTGxAfQOvhRXW/giphy.gif",
				null, "I really didn't get it", null, null, new Random()
						.nextInt(420), new Random().nextInt(420),
				new GregorianCalendar().getTime()));

		feedList.add(new Feed(
				"https://media.giphy.com/media/wOk3a0sDqGkdq/giphy.gif", null,
				"it is something", null, null, new Random().nextInt(420),
				new Random().nextInt(420), new GregorianCalendar().getTime()));
		return feedList;
	}

}
