package com.giffian.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.giffian.repository.RepositoryPackage;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

@Configuration
@EnableMongoRepositories(basePackageClasses = RepositoryPackage.class)
@ComponentScan(basePackageClasses = RepositoryPackage.class)
public class MongoConfig extends AbstractMongoConfiguration {

	private String databaseName = "test";
	private String mongoDatabaseAddress = "127.0.0.1";

	@Override
	protected String getDatabaseName() {
		return databaseName;
	}

	@Override
	public Mongo mongo() throws Exception {
		Mongo mongo = new MongoClient(mongoDatabaseAddress);
		mongo.setWriteConcern(WriteConcern.SAFE);
		return mongo;
	}

	@Override
	protected String getMappingBasePackage() {
		return "com.giffian.collection";
	}

	/*
	 * @Bean public ValidatingMongoEventListener validatingMongoEventListener()
	 * { return new ValidatingMongoEventListener(validator()); }
	 * 
	 * @Bean public LocalValidatorFactoryBean validator() { return new
	 * LocalValidatorFactoryBean(); }
	 */
}
