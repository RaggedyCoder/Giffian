package com.giffian.web.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import nz.net.ultraq.thymeleaf.LayoutDialect;

/**
 * Created by sajid on 1/1/2016.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.giffian.*")
public class WebMVCConfig extends WebMvcConfigurerAdapter {

	public static final String ENCODE_UTF_8 = "UTF-8";
	private static final Set<IDialect> additionalDialects = new HashSet<IDialect>();

	static {
		additionalDialects.add(new LayoutDialect());
	}

	public ServletContextTemplateResolver templateResolver() {
		System.out.println("templateResolver()");
		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
		templateResolver.setPrefix("/WEB-INF/views/");
		templateResolver.setSuffix(".html");
		templateResolver.setCacheable(false);
		templateResolver.setCharacterEncoding(ENCODE_UTF_8);
		return templateResolver;
	}

	public SpringTemplateEngine templateEngine() {
		System.out.println("templateEngine()");
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver());
		templateEngine.setAdditionalDialects(additionalDialects);
		return templateEngine;
	}

	@Bean
	public ThymeleafViewResolver viewResolver() {
		System.out.println("viewResolver()");
		ThymeleafViewResolver resolver = new ThymeleafViewResolver();
		resolver.setCharacterEncoding(ENCODE_UTF_8);
		resolver.setTemplateEngine(templateEngine());
		return resolver;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		System.out.println("addInterceptors(InterceptorRegistry registry)");
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("lang");
		registry.addInterceptor(localeChangeInterceptor);
		super.addInterceptors(registry);
	}

	public PageableHandlerMethodArgumentResolver pageableHandlerMethodArgumentResolver() {
		System.out.println("pageableHandlerMethodArgumentResolver()");
		PageableHandlerMethodArgumentResolver pageableHandlerMethodArgumentResolver = new PageableHandlerMethodArgumentResolver();
		pageableHandlerMethodArgumentResolver.setFallbackPageable(fallbackPageable());
		return pageableHandlerMethodArgumentResolver;
	}

	public PageRequest fallbackPageable() {
		System.out.println("fallbackPageable()");
		return new PageRequest(0, 20);
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		System.out.println("addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers)");
		super.addArgumentResolvers(argumentResolvers);
		argumentResolvers.add(pageableHandlerMethodArgumentResolver());
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		System.out.println("addResourceHandlers(ResourceHandlerRegistry registry)");
		super.addResourceHandlers(registry);
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		System.out.println("configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)");
		configurer.enable();
	}
}
