package com.giffian.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.giffian.security.UserAuthenicationProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(userAuthenicationProvider());
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(userAuthenicationProvider());
		super.configure(auth);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.authorizeRequests().antMatchers("/").permitAll();
		http.formLogin().loginPage("/login").loginProcessingUrl("/login?href=loginPage").defaultSuccessUrl("/")
				.usernameParameter("username").passwordParameter("password");
		http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/j_spring_security_logout"))
				.logoutSuccessUrl("/login?href=logOutButton").deleteCookies("JSESSIONID").invalidateHttpSession(true);
		super.configure(http);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
		super.configure(web);
	}

	public UserAuthenicationProvider userAuthenicationProvider() {
		return new UserAuthenicationProvider();
	}

	@Bean
	public MessageDigestPasswordEncoder messageDigestPasswordEncoder() {
		return new MessageDigestPasswordEncoder("sha-256", true);
	}
}
