package com.giffian.web.initializer;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.giffian.web.config.MongoConfig;
import com.giffian.web.config.WebMVCConfig;
import com.giffian.web.config.WebSecurityConfig;

//@Configuration
//@ComponentScan(basePackages = "com.giffian.*")
//@Import({ WebMVCConfig.class, MongoConfig.class, WebSecurityConfig.class })
public class AppConfiguration {

}
