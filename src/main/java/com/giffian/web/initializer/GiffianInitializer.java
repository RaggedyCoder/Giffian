package com.giffian.web.initializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.giffian.web.config.MongoConfig;
import com.giffian.web.config.WebMVCConfig;
import com.giffian.web.config.WebSecurityConfig;

/**
 * Created by sajid on 1/1/2016.
 */
public class GiffianInitializer implements WebApplicationInitializer {

	public GiffianInitializer() {
		// super(WebMVCConfig.class, MongoConfig.class);
	}

	@Override
	public void onStartup(ServletContext container) throws ServletException {
		AnnotationConfigWebApplicationContext annotationConfigWebApplicationContext = new AnnotationConfigWebApplicationContext();
		annotationConfigWebApplicationContext.register(WebMVCConfig.class, MongoConfig.class, WebSecurityConfig.class);
		annotationConfigWebApplicationContext.setServletContext(container);
		ServletRegistration.Dynamic servlet = container.addServlet("dispatcher",
				new DispatcherServlet(annotationConfigWebApplicationContext));
		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
	}
}
